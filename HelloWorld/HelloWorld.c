/*===========================================================================*/
/**
 *  @file HelloWorld.c
 *
 *   First program to compile 
 *
 *  @version %version: %
 *  @author  %derived_by:Miguel Miranda %
 *  @date    %date_modified: June 14 18:38:00 2019 %
 *
 *- -----------------------------------------------------------------------------
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *- -----------------------------------------------------------------------------
 *
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include <stdlib.h>
#include <stdio.h>

int main()
{
	printf("Hello World \n");
	return 0;
}
/*===========================================================================*/
/*!
 * @file HelloWorld.c
 *
 * @section RH REVISION HISTORY (top to bottom: first revision to last revision)
 *
 * - 14-June-2019 Miguel Miranda (ing.mtma@gmail.com)
 *   - Created initial file.
 *
 *
 */
/*===========================================================================*/

