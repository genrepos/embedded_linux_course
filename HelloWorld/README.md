#HelloWorld

In this first practice we will find out how to use the GCC compiler
and run/load a process to print a "hello world" string. 

##Compile

In order to compile a src file you will need to call the compiler and then pass
the src file PATH as asrgument and use the option "-o" to name the executable object file.

`$gcc HelloWorld.c -o Hello`

For more information about gcc options have a look at man gcc.
