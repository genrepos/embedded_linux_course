# Embedded_Linux_Course

This repo host all the contents required to take the embedded Linux Course. 
The following instructions (steps) are divided in Student and Developer sections.
 -  [Student](#stud): This section is only for the enthusiastic padawans who wants to learn about this great OS called Linux. 
 -  [Developer](#dev):If you want to give some love for people who wants to learn, feel free to add some code or documentation following these steps to add your changes.
 


# Student
>"The boy. He your son?"

>"No. He is my Padawan. Something between a student and an apprentice."

Welcome!, the objective of this course is to give you a basic understanding of the OS Linux in order to be applied in a Embedded System. The *Linux_Course.pdf* is a reference on what you are going to see in the course. 

Each of the Labs are divided in Subdirectories with the name of the Lab, you can find the compilation steps and notes in each source file.
Have fun :) 
## Prerequisites
- Before taking this course it is mandatory that the student has a Linux
distribution running in its Host-machine (native || VM).
- Basic knowledge of C programming.
- A lot of patience. 

### Installation (Clone)
If you are not familiar with git it's a good opportunity to get started.
Be sure you have installed GIT already
```
$ which git
/usr/local/bin/git
```
If not...
For Ubuntu:
```
$ sudo apt update
$ sudo apt install git
```
For Fedora:
```
$ sudo dnf -y update
$ sudo dnf -y install git
```
If your Linux distro is not listed ask to be added or use google :)

Clone the repo:
```
$ git clone https://gitlab.com/genrepos/embedded_linux_course.git
$ cd embedded_linux_course
```



# Developer
working on it...

# Important!
For the course of June 15 be aware that maybe it will be updates you may not have before the class 
you can wait for the .tar.gz i will share in class or wait to clone the repo by morning of June 15. 
Thanks!
