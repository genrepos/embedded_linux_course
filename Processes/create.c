/*===========================================================================*/
/**
 *  @file create.c
 *
 *   This is the Lab for processes - Creation/Termination
 *
 *  @version %version: %
 *  @author  %derived_by:Miguel Miranda %
 *  @date    %date_modified: June 14 18:08:00 2019 %
 *
 *- -----------------------------------------------------------------------------
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *- -----------------------------------------------------------------------------
 *
 *  @section DESC DESCRIPTION: Because this Lesson involves to many examples it is
 *  splitted aiming to be a good tool for the exposer.
 *  Bellow is the list of DEBUG options according to the Linux 
 *  presentation and also an example of how you'll need to compile
 *  this file.
 * 
 *  OPTIONS:                  Description:
 *  1) system()        Compile the SysCall "system" example.
 *  2) fork()          Compile the SysCall "fork"  zombie example.
 *  3) fork()          Compile the SysCall "fork"  orphan example.
 * 
 *  E.g:
 *        $gcc create.c -o create -D DEBUG=1
 * 
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include <stdlib.h>
#include <stdio.h>

/* Required by fork() */
#include <sys/types.h>
#include <unistd.h>
int main()
{

/**
 * system - start a new process inside another program.
 * proto:  int system(const char *command);
 * For more info use: man system
 *
 * system() system call runs the command passed to it as
 * a string and waits for it to complete.
 * The command is executed as if the command has been 
 * given to a shell.
 *
 * Desc: When you run this program you will see that the
 * PPID of sleep will be the PID of create.
 */
#if DEBUG == 1 /* system() */
   printf("Running sleep with system()\n");
   system("sleep 240");
   printf("Done.\n");

/**
 * fork - start a new process inside another program.
 * proto:  pid_t fork(void);
 * For more info use: man fork
 *
 * fork() creates  a  new process by duplicating the calling process.
 * The new process, referred to as the child, is an exact duplicate of
 * the calling process, referred to as the parent.
 *
 * On success, the PID of the child process is returned in the parent, and
 * 0  is returned in the child.  On failure, -1 is returned in the parent,
 * no child process is created, and errno is set appropriately.
 *
 *
 * Desc: 
 * DEBUG (2): In this program we will create a zombie
 * exiting immediately when we are in the child process.
 * Meanwhile, in parent process we will sleep() to be able
 * to see what happens when a process exits but its exit/return
 * value is not readed (OS keeps an entry in the Process Table).
 *
 * Then, we will wait() for the child process i.e read its exit 
 * value. 
 *
 * DEBUG (3): With the next program we want to create an
 * orphan so we will kill the parent process, in the meantime
 * child is sleep() allowing us to see how its PPID has chanded.
 * Then, child exits() normally.
 *
 * PPID of sleep will be the PID of create.
 */
#elif DEBUG == 2 || DEBUG == 3
   pid_t pid;
   int status;

   printf("Creating a child process with fork()\n");
   if ((pid = fork()) < 0) {
      perror("fork");
      exit(1);
   }
    
#if DEBUG == 2 /* Make the child a zombie */
   /* Child */
   if (pid == 0)
      exit(0);

    /* Parent */ 
   sleep(60);

    /**
     * Performing a wait allows the system 
     * to release the resources associated with the child; if a wait is not 
     * performed, then the terminated child remains in a "zombie" state.
	 * 
	 * Thanks to sleep() we can now see the child(zombie) process with "ps or top"
     */
    pid = wait(&status);
    if (WIFEXITED(status))
       fprintf(stderr, "[%d]\tProcess %d exited with status %d.\n",
       	(int) getpid(), pid, WEXITSTATUS(status));

#elif DEBUG == 3 /* Make the child an orphan */

   /* Child */
   if (pid == 0) {
	  sleep(120);
      exit(0);
   } 
   
   printf("Make the child an orphan\n");
   	/* Parent */
	return 0;
 	/**
	 * Parent exits before, so the child is now an orphan (its parent died) and the 
	 * init process will adopt it. The init process has a special handler registered 
	 * to be notified when a children dies, so when the child dies it will wait for 
	 * it to read its status code and therefore the child process does not become a zombie.
	 */

	/**
	 * Summary:
	 *
	 * Zombie: A zombie process or defunct process is a process that has completed execution but still has an entry 
	 * in the process table. This entry is still needed to allow the parent process to read its child’s exit status.
	 * The term zombie process derives from the common definition of zombie "an undead person". In the term’s metaphor,
	 * the child process has “died” but has not yet been “reaped”. Also, unlike normal processes,
	 * the kill command has no effect on a zombie process, you can't kill what's already DEAD.
	 *
	 * Orphan: A process whose parent process has finished or terminated, though it remains running itself.
	 * In a Unix-like operating system any orphaned process will be immediately adopted by the special init system process.
	 * This operation is called re-parenting and occurs automatically. 
	 *
	 * The latter process of reparenting change depending your terminal session and 
	 * some other factors you can have a better understanding reading the following
	 * discussion:
	 * https://unix.stackexchange.com/a/194208/239749
	 * In order to look the differences run the same program with
	 * gnome-terminal and xterminal.
	 *
	 * References:
	 * For the curious reader you can find the following link usefull...
	 * https://www.gmarik.info/blog/2012/orphan-vs-zombie-vs-daemon-processes/
	 */
#endif /* DEBUG 2 & 3 */
#endif /* DEBUG 1 */
exit(0);
}
/*===========================================================================*/
/*!
 * @file create.c
 *
 * @section RH REVISION HISTORY (top to bottom: first revision to last revision)
 *
 * - 28-May-2019 Miguel Miranda (ing.mtma@gmail.com)
 *   - Created initial file.
 *
 *
 */
/*===========================================================================*/

