/*===========================================================================*/
/**
 *  @file signals.c
 *
 *   This is the Lab for signals - enjoy...
 *
 *  @version %version: %
 *  @author  %derived_by:Miguel Miranda %
 *  @date    %date_modified:June 14 18:18:00 2019 %
 *
 *- -----------------------------------------------------------------------------
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *- -----------------------------------------------------------------------------
 *
 *  @section DESC DESCRIPTION: Because this Lesson involves to many examples it is
 *  splitted, aiming to be a good tool for the exposer.
 *  Bellow is the list of DEBUG options according to the Linux 
 *  presentation and also an example of how you'll need to compile
 *  this file.
 * 
 *  OPTIONS:                  Description:
 *  1) catch SIGINT        Compile the sigaction "SIGINT" example.
 *  2) Block SIG_DFL       Compile the Block signal example.
 *  2) Send a Signal       Compile the send signal  example.
 * 
 *  E.g:
 *        $gcc signals.c -o signals -D DEBUG=1
 * 
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

/**
 * sigaction - examine and change a signal action.
 * proto:  int sigaction(int signum, const struct sigaction *act,
 *                    struct sigaction *oldact);
 * For more info use: man sigaction(2), signal(7)
 *
 * sigaction() ystem call is used to change the action taken by a
 * process on receipt of a specific signal.  (See signal(7) for an
 * overview of signals.).
 *
 * Desc: When you run this program you will catch SIGINT
 * which is a signal from shell =>  ^C
 * look the behavior when you hit  ^Z
 */
#if DEBUG==1
void handler(int sig)
{
/**
 * Signal Handler Notes:
 * It is preferable to write simple signal handlers. One important reason
 * for this is to reduce the risk of creating race conditions.
 *
 * The signal that caused its invocation(i.e. signal handler) can be 
 * automatically added to the signal mask this depends on the flags used 
 * when the handler is established.
 *
 * Note: 
 * - Not all system calls and library functions can be safely called 
 * from a signal handler, .
 * 
 * - Ensure that the code of the signal handler itself is reentrant 
 * and that it calls only async-signal-safe functions.
 *
 * When a function is nonreentrant?
 * - if they use static data structures for their
 * internal bookkeeping.
 *
 * - because they return information using statically allocated memory.
 *
 * - If a signal handler updates programmer-defined global data structures
 * that are also updated within the main program.
 */
	printf("\nYou got an email", sig);/* This is a nonreentrant function */
}

int main()
{
	/**
	 * In order to catch a signal you will need to use the 
	 * data structure "sigaction"
	 * Which at least you will use the following members:
	 *
	 * void (*) (int) sa_handler  \* function @ or SIG_DFL (default action) or SIG_IGN (ignore this signal)
	 * sigset_t sa_mask           \* set of signals to block during invocation of sa_handler
	 * int sa_flags	              \* it controls how the signal is handled
	 *
	 * Notes: SIG_DFL -> informs the kernel that there is no user signal handler for the given signal
	 *					 and the kernel should take default action for it.
	 */				   
	struct sigaction act;
	act.sa_handler = handler; /* signal handler (*)() */
	/**
	 * The collection of signals that are currently blocked is called the 
	 * signal mask. Each process has its own signal mask. 
	 * When you create a new process it inherits its parent’s mask. 
	 * You can block or unblock signals with total flexibility by modifying 
	 * the signal mask.
	 */
	sigemptyset(&act.sa_mask); 	 /* Initialize a signal set to be empty i.e. No signals would be masked
								    i.e all signals will be received. */
	act.sa_flags = 0; /* setting the bitwise OR of zero, sigaction(2) for more info. */
	sigaction(SIGINT, &act, 0); /* Specify the action when SIGINT is received */
	
	

	while(1) {
		printf("Sleeping...!\n");
		sleep(1);
	}
	/* TODO
	 * The next program should be the demostration of signals added by sigmask i.e
	 * inhered sigmask 
	 * and how to send a signal to another process.
	 */
} 

/**
 * sigprocmask  - examine or change the calling process’s signal mask.
 * proto:   int sigprocmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset)
 * 
 * For more info use: man sigprocmask(2)
 *
 * Desc: Block SIGTSTP 
 * you can kill me with ^+\ 
 */
#elif DEBUG==2
void handler(int sig)
{
	printf("\nYou got an email", sig);/* This is a nonreentrant function */
}

int main()
{			   
	struct sigaction act;
	act.sa_handler = handler;
	/**
	 * You must always initialize the signal set before using it 
	 * in any other way.
	 * It's not wise to put into your program an assumption that 
	 * the system has no signals aside from the ones you know about.
	 */
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGTSTP); /* Add SIGTSTP to the signal set */
	act.sa_flags = 0; /* setting the bitwise OR of zero, sigaction(2) for more info. */
	sigprocmask(SIG_BLOCK, &act.sa_mask, NULL);/* Block SIGTSTP  */
	sigaction(SIGINT, &act, 0); /* Specify the action when SIGINT is received */	
	while(1) {
		printf("Sleeping...!\n");
		sleep(1);
	}
} 

/**
 * kill  - send a signal to another process.
 * proto:   int kill(pid_t pid , int sig );
 * 
 * For more info use: man sigprocmask(2)
 *
 * Desc: send a signal to another process using the kill() system call 
 * you can kill me with ^+\ 
 */
#elif DEBUG==3

int main(int argc, char *argv[])
{			   
	pid_t PID=0;
	if (argc < 2)
	{
		printf("You missed the PID\n");
		exit(EXIT_FAILURE);
	}
	PID = (pid_t) atoi(argv[1]);
	printf("Sending a KILL signal\n");
	/**
	 * The term kill was chosen because the
	 * default action of most of the signals 
	 * that were available on early UNIX implementa-
	 * tions was to terminate the process.
	 */
	kill(PID,SIGKILL);
} 
#endif
/*===========================================================================*/
/*!
 * @file signals.c
 *
 * @section RH REVISION HISTORY (top to bottom: first revision to last revision)
 *
 * - 14-June-2019 Miguel Miranda (ing.mtma@gmail.com)
 *   - Created initial file.
 *
 *
 */
/*===========================================================================*/
