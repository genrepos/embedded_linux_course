/*===========================================================================*/
/**
 *  @file fileIO.c
 *
 *   This is the Lab for File I/O 
 *
 *  @version %version: %
 *  @author  %derived_by:Miguel Miranda %
 *  @date    %date_modified:Thu Nov 15 12:08:00 2018 %
 *
 *- -----------------------------------------------------------------------------
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *- -----------------------------------------------------------------------------
 *
 *  @section DESC DESCRIPTION: Because this Lesson involves to many examples it is
 *  splited aiming to be a good tool for the exposer.
 *  Bellow is the list of DEBUG options according to the Linux 
 *  presentation and also an example of how you'll need to compile
 *  this file.
 *
 * OPTIONS:          Description:
 * 1) write()        Compile the SysCall "write" example.
 * 2) read()         Compile the SysCall "read"  example.
 * 3) open/close()   Compile the SysCall "open/close" example.
 * NA) ioctl()       In order to show the use of this syscall() we are
 * going to use the code implemented for "LAB - Cross Compilation"
 *
 * E.g:
 *       $gcc fileIO.c -o fileIO -D DEBUG=1
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include <unistd.h>
#include <stdlib.h>

/* These are needed for DEBUG=3 */
#include <fcntl.h>
#include <stdio.h>
int main()
{

/**
 * write - write to a file descriptor
 * proto:  ssize_t write(int fd, const void *buf, size_t count); 
 * For more info use: man 2 write
 *
 * File Descriptors (fd) are small integers that you can use to access files or
 * devices, when a program starts it usually has three of these descriptors,
 * here, we are going to WRITE to Standard Output STDOUT.
 */

#if DEBUG == 1
if ((write(1, "Writing to Standard output\n", 26)) != 26)
   write(2, "A write error has occurred on file descriptor 1\n",46); /* remember always look for the ret val*/

/**
 * read - read from a file descriptor
 * proto:  ssize_t read(int fd, void *buf, size_t count);
 * For more info use: man 2 read
 *
 * We are going to read the Standard Input fd STDIN, and put it on 
 * STDOUT, basically an "echo" program. 
 * To run this test you will need send something to the fd 0 of this program
 * so, we are going to use the sh redirection.
 * 
 * E.g:
 *       $./fileIO < text.txt
 */

#elif DEBUG == 2
   char buffer[256];
   int bread;
   bread = read(0, buffer, 256);
   if (bread == -1)
      write(2, "error ocurred when read()\n", 25);
   if ((write(1,buffer,bread)) != bread)
      write(2, "error ocurred when write()\n",26);

/**
 * open/close - create and destroy a file descriptor
 * proto:  int open(const char *pathname, int flags); int close(int fd);
 * For more info use: man 2 open/close
 *
 * open() Set an access path to a file or device, if success it returns a fd that can
 * be used in other SysCalls.
 * So... does two processes can write to the same fd? 
 * We are going to talk about this later when we see some of the sync tools that linux has. 
 *
 * close() Frees the fd and can be reused. 
 *
 * Desc: You will get a new fd for the opened file and in the $PATH
 * you will find a new file called "wFile" with "rw" permissions 
 * for "owner". 
 */

#elif DEBUG == 3
   int fd = 0;
 /*      Pathname       |     flags             |        mode        |        
  *   Path & name of    | Open for write        |  Permissions W/R   |
  *   the file          | create if necessary   |  by owner          |
  */
   fd = open("wFile",           O_WRONLY|O_CREAT,       S_IRUSR|S_IWUSR); /* What if 2 processes want to create a file? */
   if (0 > fd)
   {
      perror("fd can't be oppened");
   }
   else
   {
      printf("Fd is %d\n",fd);
      write(fd, "This is a writing test",22);
      close(fd);
   }
 /**
  * Ensure the process to be the creator of the file with the O_EXCL option
  * This protects against two programs creating the file at
  * the same time. If the file already exists, open will fail.
  * 
  * open("wFile",           O_WRONLY|O_CREAT|O_EXCL,       S_IRUSR|S_IWUSR);
  */
#endif
/* Exit in a exlplicitly way */
exit(0);
}
/*===========================================================================*/
/*!
 * @file fileIO.c
 *
 * @section RH REVISION HISTORY (top to bottom: first revision to last revision)
 *
 * - 28-May-2019 Miguel Miranda (ing.mtma@gmail.com)
 *   - Created initial file.
 *
 *
 */
/*===========================================================================*/
